# Maintainer: Mark Wagie <mark@manjaro.org>
# Maintainer: Georg Wagner <puxplaying_at_gmail_dot_com>
# Contributor: realqhc <https://github.com/realqhc>
# Contributor: Brett Alcox <https://github.com/brettalcox>
# Contributor: Fabian Bornschein <fabiscafe@archlinux.org>
# Contributor: Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor: Jan de Groot <jgc@archlinux.org>

# Ubuntu patches:
# https://salsa.debian.org/gnome-team/gnome-control-center/-/tree/ubuntu/latest/debian/patches/ubuntu

pkgbase=gnome-control-center-x11-scaling
pkgname=(
  gnome-control-center-x11-scaling
  gnome-keybindings-x11-scaling
)
_pkgname=gnome-control-center
pkgver=47.4
pkgrel=1
pkgdesc="GNOME's main interface to configure various aspects of the desktop with X11 fractional scaling patch"
url="https://apps.gnome.org/Settings"
license=(GPL-2.0-or-later)
arch=(x86_64)
depends=(
  accountsservice
  bolt
  cairo
  colord-gtk4
  cups-pk-helper
  dconf
  fontconfig
  gcc-libs
  gcr-4
  gdk-pixbuf2
  glib2
  glibc
  gnome-bluetooth-3.0
  gnome-color-manager
  gnome-desktop-4
  gnome-online-accounts
  gnome-settings-daemon
  gnome-shell
  gnutls
  graphene
  gsettings-desktop-schemas
  gsound
  gst-plugins-good
  gtk4
  hicolor-icon-theme
  json-glib
  krb5
  libadwaita
  libcolord
  libcups
  libepoxy
  libgoa
  libgtop
  libgudev
  libibus
  libmalcontent
  libmm-glib
  libnm
  libnma-gtk4
  libpulse
  libpwquality
  libsecret
  libsoup3
  libwacom
  libx11
  libxi
  libxml2
  pango
  polkit
  smbclient
  sound-theme-freedesktop
  tecla
  udisks2
  upower
)
makedepends=(
  docbook-xsl
  git
  glib2-devel
  meson
  modemmanager
)
checkdepends=(
  python-dbusmock
  python-gobject
  xorg-server-xvfb
)
source=(
  "git+https://gitlab.gnome.org/GNOME/gnome-control-center.git?signed#tag=${pkgver/[a-z]/.&}"
  "git+https://gitlab.gnome.org/GNOME/libgnome-volume-control.git"
  'https://gitlab.manjaro.org/packages/extra/gnome-control-center/-/raw/master/software-updates.patch'
  'https://gitlab.manjaro.org/packages/extra/gnome-control-center/-/raw/master/maia-accent-color-support.patch'
  'https://gitlab.manjaro.org/packages/extra/gnome-control-center/-/raw/master/manjarolinux-text-rounded.svg'
  'https://gitlab.manjaro.org/packages/extra/gnome-control-center/-/raw/master/manjarolinux-text-dark-rounded.svg'
#  'https://raw.githubusercontent.com/puxplaying/gnome-control-center-x11-scaling/c3a04acf0cd0f9f48c938c6b34b391d9aedb2b58/display-Allow-fractional-scaling-to-be-enabled.patch'
#  'https://raw.githubusercontent.com/puxplaying/gnome-control-center-x11-scaling/c3a04acf0cd0f9f48c938c6b34b391d9aedb2b58/display-Support-UI-scaled-logical-monitor-mode.patch'
  'https://salsa.debian.org/gnome-team/gnome-control-center/-/raw/8e42f5c07895c5e65249207c487d418f6f4b9c68/debian/patches/ubuntu/display-Allow-fractional-scaling-to-be-enabled.patch'
  'https://salsa.debian.org/gnome-team/gnome-control-center/-/raw/8e42f5c07895c5e65249207c487d418f6f4b9c68/debian/patches/ubuntu/display-Support-UI-scaled-logical-monitor-mode.patch'
)
sha256sums=('48792ff604ea7ff6b5e4db1bff4a4364c1c7e42f2a77894ca0af481d16208b40'
            'SKIP'
            'cafa53ec8988ef2da2a4b5aabaff05a1a60a053523b1cc854ed1054991894372'
            'a03f8e81e0eebf4528e8984a51bc1b7ce3a058dedfa8b68167f140a36895de1b'
            '1a7e96218a9d75243f65b6689f4e21220dd25c9b6610a4a9ffed815c2b8a9ea1'
            '5d49f926a6b05fd9a8a9e4f5478bb47c94e9e5b62dfa14913341104b3a454da7'
            '87909e10ee80b768079db6a1fec620de3ad69b3d63d4ceb49800b1f1b9aee4c8'
            'b021a7e19e7159a1afc3f933dedf8597ac2c94414aec0542295cee49099a8d85')
validpgpkeys=(
  9B60FE7947F0A3C58136817F2C2A218742E016BE # Felipe Borges (GNOME) <felipeborges@gnome.org>
)

prepare() {
  cd $_pkgname

  git submodule init subprojects/gvc
  git submodule set-url subprojects/gvc "$srcdir/libgnome-volume-control"
  git -c protocol.file.allow=always -c protocol.allow=never submodule update

  # Use Pamac instead of GNOME Software for software updates if installed
  patch -Np1 -i ../software-updates.patch

  # Add Maia accent color support
  patch -Np1 -i ../maia-accent-color-support.patch

  # Support UI scaled logical monitor mode (Marco Trevisan, Robert Ancell)
  patch -p1 -i "${srcdir}/display-Support-UI-scaled-logical-monitor-mode.patch"
  patch -p1 -i "${srcdir}/display-Allow-fractional-scaling-to-be-enabled.patch"
}

build() {
  local meson_options=(
    -D documentation=true
    -D location-services=enabled
    -D malcontent=true
    -D distributor_logo=/usr/share/icons/manjaro/manjarolinux-text-rounded.svg
    -D dark_mode_distributor_logo=/usr/share/icons/manjaro/manjarolinux-text-dark-rounded.svg
  )

  arch-meson $_pkgname build "${meson_options[@]}"
  meson compile -C build
}

check() {
  GTK_A11Y=none dbus-run-session xvfb-run -s '-nolisten local +iglx -noreset' \
    meson test -C build --print-errorlogs
}

_pick() {
  local p="$1" f d; shift
  for f; do
    d="$srcdir/$p/${f#$pkgdir/}"
    mkdir -p "$(dirname "$d")"
    mv "$f" "$d"
    rmdir -p --ignore-fail-on-non-empty "$(dirname "$f")"
  done
}

package_gnome-control-center-x11-scaling() {
  conflicts=($_pkgname)
  provides=($_pkgname)

  depends+=(gnome-keybindings-x11-scaling)
  optdepends=(
    'fwupd: device security panel'
    'gnome-remote-desktop: screen sharing'
    'gnome-user-share: WebDAV file sharing'
    'malcontent: application permission control'
    'networkmanager: network settings'
    'openssh: remote login'
    'power-profiles-daemon: power profiles'
    'rygel: media sharing'
    'system-config-printer: printer settings'
  )

  meson install -C build --destdir "$pkgdir"

  install -Dm644 "$srcdir"/manjarolinux{-text,-text-dark}-rounded.svg -t \
    "$pkgdir/usr/share/icons/manjaro/"

  cd "$pkgdir"
  _pick gkb usr/share/gettext/its/gnome-keybindings.*
  _pick gkb usr/share/gnome-control-center/keybindings
  _pick gkb usr/share/pkgconfig/gnome-keybindings.pc
}

package_gnome-keybindings-x11-scaling() {
  conflicts=(gnome-keybindings)
  provides=(gnome-keybindings)
  
  pkgdesc="Keybindings configuration for GNOME applications"
  depends=()

  mv gkb/* "$pkgdir"
}
